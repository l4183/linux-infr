# Ansible playbooks to create "production" ready server

## Warning

This project is currently under active development, so occasional breaking changes may arise

## Preconfig

- Copy ```templates/*``` into root folder of this project
- Change name of the ```host_vars/hostname.yml``` file together with host entry in ```inventory/hosts.yml```
- ```roles/containertemp``` is copied as well, use it to create your new docker container role

### Variables

- Go through ```host_vars/hostname.yml``` and adjust variables
- Put your passwords in ```group_vars/all.yml```
- ```inventory/hosts.yml``` uses root user by default, has to be changed after new user creation

## Security

```group_vars/all.yml``` could be de/encrypted using ```ansible-vault decrypt``` and ```ansible-vault encrypt``` before and after playbook usage

## Running

- To run playbooks:

```ansible-playbook run.yml```

- For consecutive runs, if you only want to update the Docker containers, you can run the playbook like this:

```ansible-playbook run.yml --tags traefik``` or ```ansible-playbook run.yml --tags container``` to update all enabled container on host(s)

- To limit playbook execution to single host use ```--limit``` flag

```ansible-playbook run.yml --tags traefik --limit hostname```

- Take a look at ```run.yml``` and ```containers.yml``` to find available tags

Inspired by Wolfgang a.k.a notthebee https://github.com/notthebee/infra (project has changed significantly)
