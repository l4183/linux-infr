---
- name: Include OS-specific variables
  ansible.builtin.include_vars: "{{ ansible_facts['os_family'] }}.yml"

- name: Install required system packages
  ansible.builtin.package:
    state: present
    name: "{{ docker_deps }}"
  become: true

###
- name: Docker block
  when: ansible_facts['distribution'] == 'Debian' # hast to be Debian based
  become: true
  block:
    - name: Remove old docker apt key
      ansible.builtin.apt_key:
        url: "https://download.docker.com/linux/{{ ansible_facts['distribution'] | lower }}/gpg"
        state: absent

    - name: Remove old docker repo
      ansible.builtin.apt_repository:
        repo: "deb https://download.docker.com/linux/{{ ansible_facts['distribution'] | lower }}
         {{ ansible_facts['distribution_release'] | lower }} stable"
        state: absent

    - name: Add new docker apt key
      ansible.builtin.apt_key:
        url: "https://download.docker.com/linux/{{ ansible_facts['distribution'] | lower }}/gpg"
        keyring: "/etc/apt/trusted.gpg.d/docker.gpg"

    - name: Add new docker repo
      ansible.builtin.apt_repository:
        repo: "deb [signed-by=/etc/apt/trusted.gpg.d/docker.gpg] https://download.docker.com/linux/{{ ansible_facts['distribution'] | lower }}
         {{ ansible_facts['distribution_release'] | lower }} stable"
        filename: "docker-{{ ansible_facts['distribution_release'] | lower }}"

- name: Debian specific
  when: ansible_facts['distribution'] == 'Debian' # hast to be Debian based
  become: true
  block:
    - name: Add backports GPG key (for libseccomp2)
      ansible.builtin.apt_key:
        keyserver: keyserver.ubuntu.com
        id: "{{ item }}"
      loop:
        - 04EE7237B7D453EC
        - 648ACFD622F3D138

    - name: "Add backports repository"
      ansible.builtin.apt_repository:
        repo: "deb http://deb.debian.org/debian
         {{ ansible_facts['distribution_release'] }}-backports main contrib non-free"

    - name: Update to the latest version of libseccomp2
      ansible.builtin.apt:
        update_cache: true
        name: libseccomp2
        default_release: "{{ ansible_facts['distribution_release'] }}-backports"

    - name: Switch to pip3
      community.general.alternatives:
        name: pip
        link: /usr/bin/pip
        path: /usr/bin/pip3

    - name: Switch to python3
      community.general.alternatives:
        name: python
        link: /usr/bin/python
        path: /usr/bin/python3

- name: Update required docker packages
  become: true
  ansible.builtin.apt:
    update_cache: true
    name: "{{ docker_packages }}"
    state: present

###
- name: Ensure group docker exists
  ansible.builtin.group:
    name: docker
    state: present

###
- name: Add user to docker group
  become: true
  ansible.builtin.user:
    name: '{{ username }}'
    groups:
      - docker
    append: true

- name: Reset ssh connection to allow user changes to affect 'current login user'
  ansible.builtin.meta: reset_connection

##
- name: Make sure Docker is running and enabled
  ansible.builtin.service:
    name: docker
    state: started
    enabled: true

- name: Create docker traefik network
  community.docker.docker_network:
    name: "{{ traefik_config.docker_network }}"
  when: enable_traefik | default(False)

###
- name: Handle persistent data
  become: true
  block:
    - name: Check if the persistent data folder exists
      ansible.builtin.stat:
        path: "{{ docker_dir }}"
      register: persistent_data

    - name: Check if the persistent data folder is empty
      ansible.builtin.find:
        paths:
          - "{{ docker_dir }}/"
        recurse: true
      register: persistent_data_find
      when: persistent_data.stat.exists

    - name: Create the persistent data folder
      ansible.builtin.file:
        dest: "{{ docker_dir }}"
        state: directory
        owner: "{{ username }}"
        group: "{{ groupname }}"
        mode: "0775"
        recurse: true
      when: not persistent_data.stat.exists

# ### check and sync docker data from backup
# - name: Handle docker data backup
#   block:
#     - name: Check if docker data backup exists
#       ansible.builtin.stat:
#         path: "{{ backup_dir }}/{{ docker_dir }}"
#       register: docker_data_backup

#     - name: Restore the docker data backup
#       ansible.posix.synchronize:
#         src: "{{ backup_dir }}/{{ docker_dir }}/"
#         dest: "{{ docker_dir }}/"
#         recursive: true
#       delegate_to: "{{ inventory_hostname }}"
#       when: docker_data_backup.stat.exists

#     - name: Chmod the docker data folder
#       ansible.builtin.file:
#         dest: "{{ docker_dir }}"
#         state: directory
#         owner: "{{ username }}"
#         group: "{{ groupname }}"
#         recurse: true
#       when: docker_data_backup.stat.exists
#       become: true
